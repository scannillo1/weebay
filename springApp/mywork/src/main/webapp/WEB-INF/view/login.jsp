<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>weeBay</title>
	<link href="/css/login.css" rel="stylesheet">
</head>
<body>
	<div class="wrapper">
    	<form class="form-signin" method="POST"> 
    		<div class="headmiddle">      
      			<h2 class="form-signin-heading">Please login</h2>
      		</div>
      		<div class="edgeBlank">
      			<input class="blank" name="username" type="text" class="form-control" placeholder="Username"  />
      		</div>
      		<div class="edgeBlank">
      			<input class="blank" type="password" class="form-control" name="password" placeholder="Password" /></br>      
      		</div>
      		<div class="distance"></div>
      		<button class="btn" type="submit">Login</button>   
    	</form>
  	</div>
</body>
</html>