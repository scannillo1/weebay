import org.junit.Test;

import java.time.LocalDateTime;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;

public class TestOrderProcessor {
    @Test
    public void testProcessPayment() {
        long sellerId = 12345678910L;
        LocalDateTime rightNow = LocalDateTime.now();
        Auction testAuction = new Auction(Timestamp.valueOf(rightNow), Timestamp.valueOf(rightNow), "sellerX", "itemX", 49);
        boolean paymentSuccess = TestUtils.getTestProcessor().processPayment(testAuction);
        assertEquals(paymentSuccess, true);
    }

}
