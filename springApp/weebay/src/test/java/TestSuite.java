import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestAdmin.class,
        TestCategory.class,
        TestNotifier.class,
        TestOrderProcessor.class,
        TestUser.class,
        TestItem.class
})

public class TestSuite {
} 