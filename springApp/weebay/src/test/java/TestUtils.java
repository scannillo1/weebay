
public class TestUtils {
    static User testUser1  = new User("Steve", "bajazz", "5552424", "theStreet");
    static User testUser2 = new User("Sammy", "paramore", "1234567", "theAddress");
    static Admin testAdmin = new Admin("Joseph Marcus", "jmarcus3");
    static Category testCategory = new Category("Clothes");
    static Item testItem  = new Item("601", "T Shirt", "Plain White T", false, "Steve", testCategory);
    static OrderProcessor testProcessor = new OrderProcessor("paymentKey", "shippingKey");

    public static User getTestUser1(){
        return  testUser1;
    }
    public static User getTestUser2(){
        return testUser2;
    }
    public static Admin getTestAdmin(){
        return testAdmin;
    }
    public static Item getTestItem(){
        return testItem;
    }
    public static Category getTestCategory(){
        return testCategory;
    }
    public static OrderProcessor getTestProcessor(){
        return testProcessor;
    }
}
