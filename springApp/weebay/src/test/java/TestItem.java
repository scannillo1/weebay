import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestItem {

    @Test
    public void testItem() {
        String newDescription = "shirt";
        TestUtils.testItem.updateDescription(newDescription);
        assertEquals(TestUtils.getTestItem().description, newDescription);
    }
}
