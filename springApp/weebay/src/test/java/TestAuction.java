import org.junit.Test;
import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;

public class TestAuction {
    @Test
    public void testBiddingProcess() {
        Category householdCat = new Category("household");
        Item thinganizer = new Item("the thinganizer", "it turns stuff into things", false,
                "marcus.joseph20@gmail.com", householdCat);
        Item dinganizer = new Item("the dinganizer", "it turns stuff into dings", false,
                "marcus.joseph20@gmail.com", householdCat);
        Item jazzMusic = new Item("some jazz music", "you will not listen to it but the records make good dustpans", false,
                "marcus.joseph20@gmail.com", householdCat);
        thinganizer.writeToDB();
        dinganizer.writeToDB();
        jazzMusic.writeToDB();

        Timestamp starttime = Auction.toTimeStamp(2018, 1, 1, 12, 0);
        Timestamp endtime = Auction.toTimeStamp(2018, 12, 31, 12, 0);
        Auction auction1 = thinganizer.createAuctionForItem(starttime, endtime, 10);
        Auction auction2 = dinganizer.createAuctionForItem(starttime, endtime, 10);
        Auction auction3 = jazzMusic.createAuctionForItem(starttime, endtime, 10);

        auction1.bid("steven", 10);
        auction2.bid("steven", 10);
        auction3.bid("alan", 15);

        auction1.endAuction(true);
        auction2.endAuction(true);
        auction3.endAuction(true);

        Cart stevensCart = new Cart("steven");
        Cart alansCart = new Cart("alan");

        assertEquals(20.0, stevensCart.checkout(), .001);
        assertEquals(15.0, alansCart.checkout(), .001);

    }

    @Test
    public void testBuyNowProcess() {
        Category householdCat = new Category("household");
        Item zoomerBroom= new Item("ZoomerBroom", "as seen on tv", false,
                "marcus.joseph20@gmail.com", householdCat);
        zoomerBroom.writeToDB();
        Timestamp starttime = Auction.toTimeStamp(2018, 1, 1, 12, 0);
        Timestamp endtime = Auction.toTimeStamp(2018, 12, 31, 12, 0);
        Auction auction1 = zoomerBroom.createAuctionForItem(starttime, endtime, 10);
        auction1.buyNow("steven");

        Cart stevensCart = new Cart("steven");
        assertEquals(10.0, stevensCart.checkout(),.001);
    }
}
