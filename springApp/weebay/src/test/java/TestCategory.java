import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCategory {
    @Test
    public void testCategoryCreation() {
        String newCategory = "Health";
        Category Category = new Category(newCategory);
        assertEquals(Category.name, newCategory);
    }
}
