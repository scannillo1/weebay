import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAdmin {

    @Test
    public void testItemFlagging(){
        Category drugs = new Category("Drugs");
        Item maliciousItem = new Item("123", "Drugs", "Illegal substandes.", true, "Nonsense Person", drugs);
        TestUtils.getTestAdmin().addItemToFlagList(maliciousItem); //updates flag and adds to flaglist
        assertEquals(true, maliciousItem.flag);
    }

    @Test
    public void testUserBlocking(){
        User maliciousUser = new User("Nonsense Person", "123", "xxx", "yyy");
        TestUtils.getTestAdmin().addUserToBlockList(maliciousUser); //updates flag and adds to flaglist
        assertEquals(true, TestUtils.getTestAdmin().blockList.contains(maliciousUser));
    }
}
