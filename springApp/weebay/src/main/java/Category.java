import java.util.List;

public class Category {
	// Properties
	public String name;

	// Constructor
	public Category(String name) {
		this.name = name;
	}

	// Category Accessor Methods
	public String getCategoryName() {
		return name;
	}

	// Category Functionality Methods
	public boolean writeToDB() {
		// If category already written to DB, return true.
		if (JDBCCategory.doesCategoryExist(this.name)) {
			return true;
		// If category not already written to DB, return response of creation attempt.
		} else {
			return JDBCCategory.createCategory(this.name);
		}
	}

	public void deleteCategory() {
		JDBCCategory.deleteCategory(this.name);
	}

	// Sample Code
	public static void main(String[] args) {
		// To create category & persist to DB
		Category electronicsCategory = new Category("Electronics");
		Boolean writeToDBSuccess = electronicsCategory.writeToDB();
		System.out.println("Electronics category in DB: " + writeToDBSuccess);

		// To delete a category
		Category glassesCategory = new Category("Glasses");
		glassesCategory.writeToDB();
		glassesCategory.deleteCategory();

		// Get all current categories in DB
		List<String> allCategories =JDBCCategory.getAllCategories();
		System.out.println("All categories: " + allCategories);
	}
}