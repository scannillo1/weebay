import javax.xml.soap.Text;
import java.util.List;

public class Notifier {

    // Properties
    private String notifyAPI;
    private static final Notifier singleton = new Notifier();
    private static final SendEmailAlert emailObject = new SendEmailAlert();

    public static Notifier getInstance() {
        return singleton;
    }

    // Methods
    public void alertSeller(String sellerEmail, String itemID) {
        String subject = "Update on your item "+ itemID+ " on Weebay";
        String body = "Hi user, your item has just been bid on by a customer. " +
                "Please check it on our website.";
        emailObject.sendAlert(sellerEmail, subject, body);
    }

    public void alertBidder(String bidderEmail, String itemID) {
        String subject = "Update on your bid on Item"+itemID;
        String body = "This is a notification regarding your highest bid on "+itemID+
                ". You have been outbid by another customer.";
        emailObject.sendAlert(bidderEmail, subject, body);
    }

    public void watchListAlert(String userEmail) {
        String subject = "Watchlist update Alert!";
        String body = "Hey user, your watchlist has been updated!";
        emailObject.sendAlert(userEmail, subject, body);
    }

    public void bidTimeAlert(List<String> userEmails) {
        String subject = "Auction Alert!";
        String body = "Hey user, the closing time of the auctiomn is fast approaching.";
        for (String email : userEmails){
            emailObject.sendAlert(email, subject, body);
        }
    }

}
