package com.weebay.weebay;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountPage {

    @RequestMapping("/account")
    public String index() {

        return "This is the account site.";
    }

}
