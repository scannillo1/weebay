package com.weebay.weebay;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuctionPage {

    @RequestMapping("/auction")
    public String index() {

        return "This is the auction site.";
    }
}
