package com.weebay.weebay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeebayApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeebayApplication.class, args);
	}
}
