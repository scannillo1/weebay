import java.sql.*;

public class JDBCAuction {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://mpcs53001.cs.uchicago.edu/zlyueDB?serverTimezone=UTC";

    //  Database credentials
    static final String USER = "zlyue";
    static final String PASS = "Eize7Pia";

    //creating an auction
    public static Boolean createAuction(String auctionid, Timestamp starttime, Timestamp endtime, String ownerid, String buyerid,
                                 String itemid, double price, double nowPrice) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO auction VALUES" + "('" + auctionid + "', '" + starttime + "', '" + endtime + "', '" +
                    ownerid + "','" + buyerid + "','"  +  itemid + "', '" + price + "','" + nowPrice + "')";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }

    public static Auction selectAuction(String auctionid) {
        Connection conn = null;
        Statement stmt = null;
        Auction result = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("SELECT * FROM auction WHERE auctionid='%s'", auctionid);
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 6: Clean-up environment
            while (rs.next()) {
                String aid = rs.getString("auctionid");
                Timestamp starttime = rs.getTimestamp("starttime");
                Timestamp endtime = rs.getTimestamp("endtime");
                String ownerid = rs.getString("ownerid");
                String buyerid = rs.getString("buyerid");
                String itemid = rs.getString("itemid");
                double price = rs.getInt("price");
                double nowPrice = rs.getInt("nowPrice");
                result = new Auction(aid, starttime, endtime, ownerid, buyerid, itemid, price, nowPrice);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return result;
    }

    public static boolean updateAuction(String auctionid, Timestamp starttime, Timestamp endtime, String ownerid, String buyerid,
                                 String itemid, double price, double nowPrice) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE auction SET auctionid= '" + auctionid + "', starttime= '" + starttime + "', endtime ='" + endtime +
                    "', ownerid= '" + "', buyerid= '" + buyerid + "', itemid= '" + itemid + "',price= '" + price + "', nowPrice='" + nowPrice
                    +"'WHERE auctionid = '" + auctionid + "'";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }

    public static void deleteAuction(String auctionID) {

        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("DELETE FROM auction WHERE auctionid='%s'", auctionID);
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    public static boolean bidOnAuction(String auctionid, String buyerid, double price) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE auction SET buyerid= '" + buyerid + "', price= '" + price + "'" +
                    "WHERE auctionid= '" + auctionid + "' AND price < '" + price +"'";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }

    public static void main(String[] args) {
        //JDBCAuction jdbcAuction = new JDBCAuction();
        Timestamp starttime = Auction.toTimeStamp(2018, 7, 15, 13, 0);
        Timestamp endtime = Auction.toTimeStamp(2018, 10, 10, 10, 0);
        createAuction("jfjdakfjkla", starttime, endtime, "marcus.joseph20@gmail.com", null, "an item id", 0, 15);

//        System.out.println(JDBCAuction.createAuction("asdf", "2018-01-01", "2018-12-31",
//                "masterAuctioner2", "Brady", "the  thinganator", 5, 10));
//        System.out.println(JDBCAuction.updateAuction("asdf", "2018-01-01", "2018-12-31",
//                "masterAuctioner2", "Brady", "the  thinganator", 7, 10));
        JDBCAuction.deleteAuction("jfjdakfjkla");

    }

}
