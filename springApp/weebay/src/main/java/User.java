import java.util.List;

public class User extends Account {
    public String paymentInfo;
    //public List<CartItem> cart;
    public List<Item> watchList;
    public List<Auction> auctionList;
    public List<Auction> bidList;
    public String phone;
    public String address;

    public User(String name, String password, String phone, String address) {
        this.username = name;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    // create account
    public void createAccount() {
        if (JDBCUser.selectUser(this.username) != null) {
            System.out.println("User name exists. Please choose a new one.");
            return;
        }
        JDBCUser.createUser(this.username, this.password, this.phone, this.address);
    }

    // update account
    public void updateAccount(String name, String password, String phone, String address) {
        if (JDBCUser.selectUser(name) == null) {
            System.out.println("User doesn't exist. Please check the user name and try again.");
            return;
        }
        JDBCUser.updateUser(name, password, phone, address);
    }

    public List<Auction> viewAuctionList() {
        this.auctionList = JDBCUserAuction.selectUserAuction(this.username);
        for (Auction cur : this.auctionList) {
            System.out.println(cur.getBuyerid() + " " + cur.getOwnerid() + " " + cur.getStarttime() + " " + cur.getEndtime() + " " + cur.getPrice());
        }
        return this.auctionList;
    }

    public List<Auction> viewBids() {
        this.bidList = JDBCUser.selectBids(this.username);
        for (Auction cur : this.bidList) {
            System.out.println(cur.getBuyerid() + " " + cur.getOwnerid() + " " + cur.getStarttime() + " " + cur.getEndtime() + " " + cur.getPrice());
        }
        return this.bidList;
    }

//    public void updateCart(Item item) {
//        this.cart.add(item);
//    }

    public static void main(String[] args) {
        User userTest = new User("sammy", "password", "1234567", "Crerar");
        userTest.createAccount();
        userTest.updateAccount("sammy", "password", "1234567", "Regenstein");
        User userTest2 = JDBCUser.selectUser("joey");
        List<Auction> res = userTest2.viewAuctionList();
        List<Auction> bres = userTest2.viewBids();
    }
}

