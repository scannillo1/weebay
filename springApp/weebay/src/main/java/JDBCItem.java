import java.sql.*;
import java.util.Locale;

public class JDBCItem {

    // JDBCUser driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://mpcs53001.cs.uchicago.edu/zlyueDB?serverTimezone=UTC";

    //  Database credentials
    static final String USER = "zlyue";
    static final String PASS = "Eize7Pia";

    public static void main(String[] args) {

        // Sample Add, Select, and Delete Methods

        // JDBCCategory.createCategory("Electronics");
        // Boolean result = JDBCItem.createItem("1234567", "Wood Desk","New", false, "Sammy", "Furniture");
        // Item result2 = JDBCItem.selectItemById("1234567");
        // JDBCItem.deleteItem("1271");
        // Boolean result3 = JDBCItem.updateItem("1234567", "Wood Desk", "Used", false, "Sammy", "Furniture");
    }

    public static Boolean createItem(String itemid, String itemname, String description, Boolean flag, String ownername, String cate) {
        System.out.println("Create item called.");
        Connection conn = null;
        Statement stmt = null;
        try {
            // Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            // Open DB connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Perform query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;

            int flagInt;
            if (flag == true) {
                flagInt = 1;
            } else {
                flagInt = 0;
            }

            sql = "INSERT INTO item VALUES" + "('" + itemid + "','" + itemname + "','" + description + "','" + flagInt + "','" + ownername + "','" + cate + "')";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Error handling. Return false.
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }//end try
        System.out.println("Added an item.");
        return true;
    }

    public static Item selectItemByName(String itemname) {
        Connection conn = null;
        Statement stmt = null;
        Item result = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("SELECT * FROM item WHERE itemname='%s'", itemname);
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 6: Clean-up environment
            while (rs.next()) {
                String itemid = rs.getString("itemid");
                String name = rs.getString("itemname");
                String description = rs.getString("description");
                int flagInt = rs.getInt("flag");

                Boolean flagBool;
                if (flagInt == 1) {
                    flagBool = true;
                } else {
                    flagBool = false;
                }

                String ownername = rs.getString("ownername");
                String cate = rs.getString("cate");
                Category categoryItem = new Category(cate);
                result = new Item(itemid, name, description, flagBool, ownername, categoryItem);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }//end try
        return result;
    }

    public static Item selectItemById(String itemid) {
        Connection conn = null;
        Statement stmt = null;
        Item result = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("SELECT * FROM item WHERE itemid='%s'", itemid);
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 6: Clean-up environment
            while (rs.next()) {
                String id = rs.getString("itemid");
                String itemname = rs.getString("itemname");
                String description = rs.getString("description");
                int flagInt = rs.getInt("flag");

                Boolean flagBool;
                if (flagInt == 1) {
                    flagBool = true;
                } else {
                    flagBool = false;
                }

                String ownername = rs.getString("ownername");
                String cate = rs.getString("cate");
                Category categoryItem = new Category(cate);
                result = new Item(itemid, itemname, description, flagBool, ownername, categoryItem);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }//end try
        return result;
    }

    public static void deleteItem(String itemid) {

        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("DELETE FROM item WHERE itemid='%s'", itemid);
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }//end try
        System.out.println("Deleted item #: " + itemid);
    }

    public static Boolean updateItem(String itemid, String itemname, String description, Boolean flag, String ownername, String cate) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            int flagInt;
            if (flag == true) {
                flagInt = 1;
            } else {
                flagInt = 0;
            }

            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE item SET itemname = '" + itemname + "', description = '" + description + "', flag ='" + flagInt + "', ownername ='" + ownername
                    +"'WHERE itemid = '" + itemid + "'";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return true;
    }

}
