import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCCart {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://mpcs53001.cs.uchicago.edu/zlyueDB?serverTimezone=UTC";

    //  Database credentials
    static final String USER = "zlyue";
    static final String PASS = "Eize7Pia";

    //creating an auction
    public static Boolean addToCart(String username, String itemid, double price) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO cart VALUES" + "('" + username + "', '" + itemid + "', '" + price + "')";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }

    public static List<CartItem> selectCart(String username) {
        Connection conn = null;
        Statement stmt = null;
        List<CartItem> userCart = new ArrayList<>();
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("SELECT * FROM item JOIN cart ON item.itemid = cart.itemid WHERE username='%s'", username);
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 6: Clean-up environment
            while (rs.next()) {
                String itemid = rs.getString("itemid");
                String name = rs.getString("itemname");
                String description = rs.getString("description");
                int flagInt = rs.getInt("flag");

                Boolean flagBool;
                if (flagInt == 1) {
                    flagBool = true;
                } else {
                    flagBool = false;
                }

                String ownername = rs.getString("ownername");
                String cate = rs.getString("cate");
                double price = rs.getInt("price");
                Category categoryItem = new Category(cate);
                Item item = new Item(itemid, name, description, flagBool, ownername, categoryItem);
                CartItem cartItem = new CartItem(item, price);
                //System.out.println(auction.getItemid());
                userCart.add(cartItem);
                //result = new Auction(aid, starttime, endtime, ownerid, buyerid, itemid, price, nowPrice);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return userCart;
    }

    public static void emptyCart(String username) {

        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("DELETE FROM cart WHERE username='%s'", username);
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    public static void main(String[] args) {



    }

}
