//helper class for cart
public class CartItem {
    public Item item;
    public double price;

    public CartItem(Item item, double price){
        this.item = item;
        this.price = price;
    }

    public String toString() {
        return item.itemname + " " + price;
    }
}
