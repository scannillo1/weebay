import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCUserAuction {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://mpcs53001.cs.uchicago.edu/zlyueDB?serverTimezone=UTC";

    //  Database credentials
    static final String USER = "zlyue";
    static final String PASS = "Eize7Pia";

    //creating an auction
    public static Boolean addToUserAuction(String username, String auctionid) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO userAuction VALUES" + "('" + username + "', '" + auctionid + "')" +
                    "ON DUPLICATE KEY UPDATE auctionid = auctionid";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
            return false;
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return true;
    }

    public static void removeFromUserAuction(String auctionid) {

        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("DELETE FROM userAuction WHERE auctionid='%s'", auctionid);
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }

    public static List<Auction> selectUserAuction(String username) {
        Connection conn = null;
        Statement stmt = null;
        List<Auction> bidList = new ArrayList<>();
        try {
            //STEP 2: Register JDBCUser driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = String.format("SELECT * FROM auction JOIN userAuction ON auction.auctionid = userAuction.auctionid " +
                    "WHERE username='%s'", username);
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 6: Clean-up environment
            while (rs.next()) {
                String aid = rs.getString("auctionid");
                Timestamp starttime = rs.getTimestamp("starttime");
                Timestamp endtime = rs.getTimestamp("endtime");
                String ownerid = rs.getString("ownerid");
                String buyerid = rs.getString("buyerid");
                String itemid = rs.getString("itemid");
                //System.out.println(itemid);
                int price = rs.getInt("price");
                int nowPrice = rs.getInt("nowPrice");
                Auction auction = new Auction(aid, starttime, endtime, ownerid, buyerid, itemid, price, nowPrice);
                bidList.add(auction);
                //result = new Auction(aid, starttime, endtime, ownerid, buyerid, itemid, price, nowPrice);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBCUser
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return bidList;
    }

    public static void main(String[] args) {
        //removeFromUserAuction("1effdf90-787b-400b-98cd-4a8be7f840d9");
//        List<Auction> stevensBidList = selectUserAuction("steven");
//        List<Auction> alansBidList = selectUserAuction("alan");
//        Auction[] stevensBidArray = stevensBidList.toArray(new Auction[stevensBidList.size()]);
//        Auction[] alansBidArray = alansBidList.toArray(new Auction[alansBidList.size()]);
//        System.out.println("Steven:");
//        for(int i=0; i<stevensBidArray.length; i++)
//            System.out.println(stevensBidArray[i].getItemid());
//
//        System.out.println("Alan:");
//        for(int i=0; i<alansBidArray.length; i++)
//            System.out.println(alansBidArray[i].getItemid());


    }

}
