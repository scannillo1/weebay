import java.util.ArrayList;
import java.util.List;

public class Admin extends Account {
    public List<User> blockList = new ArrayList<>();
    public List<Item> flagList = new ArrayList<>();
    public List<Auction> auctionsList = new ArrayList<>();
    public List<Category> categoriesList = new ArrayList<>();
    public CustomerService service = new CustomerService();

    public Admin(String name, String password){
        this.username = name;
        this.password = password;
    }

    public void addUserToBlockList(User user){
        this.blockList.add(user);
    }

    public List<User> viewBlockList(){
        return this.blockList;
    }

    public void addItemToFlagList(Item item){
        item.flag=true;
        this.flagList.add(item);
    }

    public List<Item> viewFlagList(){
        return this.flagList;
    }

    public List<Auction> viewAllAuctions(){
        return this.auctionsList;
    }
    public void addCategory(Category category){
        this.categoriesList.add(category);
    }

    public void removeCategory(Category category){
        this.categoriesList.remove(category);
    }


}