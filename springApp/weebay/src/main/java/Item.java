import java.util.UUID;
import java.sql.Timestamp;
public class Item {

	// fields
	public String itemid;
	public String itemname;
	public String description;
	public Boolean flag;
	public String ownername;
	public Category category;

	//constructor for creating a new one
	public Item(String itemname, String description, Boolean flag, String ownername, Category category) {
		this.itemid = UUID.randomUUID().toString();
		this.itemname = itemname;
		this.description = description;
		this.flag = flag;
		this.ownername = ownername;
		this.category = category;
	}

	// constructor for creating from database
	public Item(String itemid, String itemname, String description, Boolean flag, String ownername, Category category) {
		this.itemid = itemid;
		this.itemname = itemname;
		this.description = description;
		this.flag = flag;
		this.ownername = ownername;
		this.category = category;
	}

	// methods

	public boolean writeToDB() {
		return JDBCItem.createItem(this.itemid, this.itemname, this.description, this.flag, this.ownername, this.category.name);
	}

	public Auction createAuctionForItem(Timestamp starttime, Timestamp endtime, double nowPrice) {
		Auction auction = new Auction(starttime, endtime, this.ownername, this.itemid, nowPrice);
		auction.writeToDB();
		return auction;
	}

	// update item description
	public void updateDescription(String newDescription) {
		if (newDescription != null) {
			this.description = newDescription;
			System.out.println("Update Sucessful");
		} else {
			System.out.println("Upfate Failed");
		}
	}
}