import java.util.List;

public class Cart {
    private String username;
    private List<CartItem> cart;

    public Cart(String username) {
        this.username = username;
        this.cart = JDBCCart.selectCart(username);
    }


    public List<CartItem> getCart() {
        return cart;
    }

    public String getUsername() {
        return username;
    }

    public double checkout() {
        JDBCCart.emptyCart(this.username);
        double total = 0;
        for(int i = 0; i < this.cart.size(); i++) {
            CartItem cartItem = this.cart.get(i);
            JDBCItem.deleteItem(cartItem.item.itemid);
            total += cartItem.price;
        }
        return total;
    }
}
