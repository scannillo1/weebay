public class OrderProcessor {

    // Properties
    private String paymentAPI;
    private String shippingAPI;

    // Constructor
    public OrderProcessor(String paymentAPI, String shippingAPI) {
        this.paymentAPI = paymentAPI;
        this.shippingAPI = shippingAPI;
    }

    // Methods
    public boolean processPayment(Auction auction) {
        System.out.println("Payment process successful.");
        return true;
        // Processes payment
    }

    public boolean processShipping(Auction auction) {
        System.out.println("Shipping process successful.");
        return false;
        // Processes shipping
    }

}
