import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomerService {
    public List<String> complaints;
    public String emailID;
    Scanner scanner = new Scanner(System.in);
    public CustomerService(){
        emailID = "customer_service@weebay.com";
        complaints = new ArrayList();
    }
    public void setCustomerComplaints(){
        System.out.println("Do you have any complaints?(Y/N)");
        String response = scanner.next();
        while (response.equals("Y")){
            System.out.println("Enter Complaint");
            String complaint = scanner.nextLine();
            complaints.add(complaint);
            System.out.println("Do you have any complaints?(Y/N)");
            response = scanner.next();
        }
    }

    public List<String> getCustomerComplaints(){
        return complaints;
    }
    

}
